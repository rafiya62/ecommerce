﻿using ecomm.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.Database
{
    public class CBContext : DbContext 

    {
      public CBContext() : base("ecommConnection")
            {

            }

        public DbSet<Category>  Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
