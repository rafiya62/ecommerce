﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ecomm.Web.Startup))]
namespace ecomm.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
