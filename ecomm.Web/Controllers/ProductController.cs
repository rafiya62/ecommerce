﻿using ecomm.Entities;
using ecomm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecomm.Web.Controllers
{
    public class ProductController : Controller
    {
        ProductsServices productsService = new ProductsServices();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ProductTable(string Search)
        {
            var products = productsService.GetProduct();
            if(string.IsNullOrEmpty(Search)==false)
            {
                products = products.Where(p => p.Name != null && p.Name.ToLower().Contains(Search.ToLower())).ToList();
            }
     
            return PartialView(products);

            /*foreach (var p in products)
            {
                if(p.Name==Search)
                    //found

            }*/
        }


        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Product product)
        {
            productsService.SaveProduct(product);
            return RedirectToAction("ProductTable");
        }
    }
}